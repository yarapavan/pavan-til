### git add -p
If you have been using Git long enough, you have probably heard about git add -p/--patch, which allows you to selectively stage parts of files. However, did you know that many other Git commands support this argument as well? Among them are commit, reset, checkout, stash, and log, and they represent the main topic of the present post.


