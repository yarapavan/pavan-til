Parameters for better logging
  > git log --oneline --graph

Log actual changes in a file
 >  git log -p filename

Only Log changes for some specific lines in file
 >  git log -L 1,1:some-file.txt

Log changes not yet merged to the parent branch
 > git log --no-merges master..

Extract a file from another branch
 > git show some-branch:some-file.js

Some notes on rebasing
 > git pull --rebase

Remember the branch structure after a local merge
 > git merge --no-ff

Fix your previous commit, instead of making a new commit
> git commit --amend

Three stages in git, and how to move between them
 > git reset --hard HEAD and git status -s

Revert a commit, softly
 > git revert -n

See diff-erence for the entire project (not just one file at a time) in a 3rd party diff tool
 > git difftool -d

Ignore the white space
 > git diff -w

Only “add” some changes from a file
 > git add -p

Discover and zap those old branches
 > git branch -a

Stash only some files
 > git stash -p

Quickly find a commit that broke your feature (EXTRA AWESOME)
 > git bisect
