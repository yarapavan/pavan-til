### DataTurks
* [Trending Datasets from DataTurks](https://dataturks.com/projects/trending)
### AWS datdasets
* [AWS hosts a variety of public datasets that anyone can access for free](https://aws.amazon.com/public-datasets/). Here is a list of available Public Datasets on AWS
	* Geospatial and Environmental Datasets
		* [Landsat on AWS](http://aws.amazon.com/public-data-sets/landsat/): An ongoing collection of satellite imagery of all land on Earth produced by the Landsat 8 satellite.
		* [Sentinel-2 on AWS](https://aws.amazon.com/public-datasets/sentinel-2/): An ongoing collection of satellite imagery of all land on Earth produced by the Sentinel-2 satellite.
		* [GOES on AWS](https://aws.amazon.com/public-datasets/goes/): GOES provides continuous weather imagery and monitoring of meteorological and space environment data across North America.
		* [SpaceNet on AWS](http://aws.amazon.com/public-data-sets/spacenet/): A corpus of commercial satellite imagery and labeled training data to foster innovation in the development of computer vision algorithms.
		* [OpenStreetMap on AWS](https://aws.amazon.com/public-datasets/osm/): OSM is a free, editable map of the world, created and maintained by volunteers. Regular OSM data archives are made available in Amazon S3.
		* [MODIS on AWS](https://aws.amazon.com/public-datasets/modis/): Select products from the Moderate Resolution Imaging Spectroradiometer (MODIS) managed by the U.S. Geological Survey and NASA.
		* [Terrain Tiles](https://aws.amazon.com/public-datasets/terrain/): A global dataset providing bare-earth terrain heights, tiled for easy usage and provided on S3.
		* [NAIP](https://aws.amazon.com/public-datasets/naip/): 1 meter aerial imagery captured during the agricultural growing seasons in the continental U.S.
		* [NEXRAD on AWS](https://aws.amazon.com/public-datasets/nexrad/): Real-time and archival data from the Next Generation Weather Radar (NEXRAD) network.
		* [NASA NEX](http://aws.amazon.com/nasa/nex/): A collection of Earth science datasets maintained by NASA, including climate change projections and satellite images of the Earth's surface.
		* [District of Columbia LiDAR](https://aws.amazon.com/public-datasets/dc-lidar/): LiDAR point cloud data for Washington, DC.
		* [EPA Risk-Screening Environmental Indicators](https://aws.amazon.com/public-datasets/epa-rsei/): detailed air model results from EPA’s Risk-Screening Environmental Indicators (RSEI) model.
		* [HIRLAM Weather Model](https://aws.amazon.com/public-datasets/fmi-hirlam/): HIRLAM (High Resolution Limited Area Model) is an operational synoptic and mesoscale weather prediction model managed by the Finnish Meteorological Institute.
* Genomics and Life Science Datasets
	* [1000 Genomes Project](https://aws.amazon.com/1000genomes/): A detailed map of human genetic variation.
	* [TCGA on AWS](http://aws.amazon.com/public-data-sets/tcga/): Raw and processed genomic, transcriptomic, and epigenomic data from The Cancer Genome Atlas (TCGA) available to qualified researchers via the Cancer Genomics Cloud.
	* [ICGC on AWS](http://aws.amazon.com/public-data-sets/icgc/): Whole genome sequence data available to qualified researchers via The International Cancer Genome Consortium (ICGC).
	* [3000 Rice Genome on AWS](https://aws.amazon.com/public-data-sets/3000-rice-genome/): Genome sequence of 3,024 rice varieties.
	* [Genome in a Bottle (GIAB)](https://aws.amazon.com/public-datasets/giab/): Several reference genomes to enable translation of whole human genome sequencing to clinical practice.
* Datasets for Machine Learning
	* [Common Crawl](https://aws.amazon.com/public-data-sets/common-crawl/): A corpus of web crawl data composed of over 5 billion web pages.
	* [Amazon Bin Image Dataset](https://aws.amazon.com/public-datasets/amazon-bin-images/): Over 500,000 bin JPEG images and corresponding JSON metadata files describing products in an operating Amazon Fulfillment Center.
	* [GDELT](https://aws.amazon.com/public-datasets/gdelt/): Over a quarter-billion records monitoring the world's broadcast, print, and web news from nearly every corner of every country, updated daily.
	* [Multimedia Commons](http://aws.amazon.com/public-data-sets/multimedia-commons/): A collection of nearly 100M images and videos with audio and visual features and annotations.
	* [Google Books Ngrams](https://aws.amazon.com/datasets/google-books-ngrams/): A dataset containing Google Books n-gram corpuses.
	* [SpaceNet on AWS](http://aws.amazon.com/public-data-sets/spacenet/): A corpus of commercial satellite imagery and labeled training data to foster innovation in the development of computer vision algorithms.
* Financial Data
	* [Deutsche Börse Public Dataset](https://aws.amazon.com/public-datasets/deutsche-boerse-pds/): Real-time data derived from Deutsche Börse's trading market systems available to the public for free.
* Regulatory and Statistical Data
	* [IRS 990 Filings on AWS](https://aws.amazon.com/public-datasets/irs-990/): Machine-readable data from certain electronic 990 forms filed with the IRS from 2011 to present.
	* [ACS PUMS on AWS](https://aws.amazon.com/public-datasets/us-census-acs/): U.S. Census American Community Survey (ACS) Public Use Microdata Sample (PUMS) is available in a linked data format using the Resource Description Framework (RDF) data model.
	* [USAspending.gov on AWS](http://aws.amazon.com/public-datasets/usaspending): USAspending.gov database, which includes data on all spending by the federal government, including contracts, grants, loans, employee salaries, and more.  
