### productivity
Link: http://liveyourlegend.net/productivity-part-1-the-single-most-important-thing-to-do-to-be-massively-productive-aka-get-more-sht-done/
####Notes:

-  Notice how you say things. Do you find yourself saying “I have to go pick up the kids from school.” “I should go to this work function.” I have to go to the gym.”
-  Replace “I have to” or “I should” with “I choose to.” Because really you don’t have to do anything! If you are doing it, you are choosing to, so own up to it. Make sure you make this correction not only as you speak aloud to others but also in your head. Your thoughts dictate your actions and your actions are what determine how much you actually get done!
- Notice the freedom you feel. And you may even start to release some heaviness (and realize that you are, as Tony Robbins says, ‘should-ing all over yourself’). So if you do happen to notice this, it might be time to take some of those things off your list… It will free you and those around you!
- Once “I choose to” feels like it has become your truth, take it a step further and replace “I choose to” with “I get to.” Because I can guarantee you someone would die to be in your shoes. You have to go pick up the kids from school? There’s someone out there who would do anything to have children of their own. You should go to a work function? There’s someone out there who thinks your job is their dream job and would love to be in your shoes. You have to go to the gym? There’s someone out there whose body does not allow them to go to the gym, so be grateful and honor the body you have. You “get to” do so many amazing things. Appreciate them!
- Once “I get to” feels like it has become your truth, take it a step further and replace “I get to” with “I want to.” Because when you can say this with genuine authenticity, you will want to!

Link: http://liveyourlegend.net/productivity-part-2-10-practical-tips-to-maximize-productivity-aka-get-more-sht-done/
> “If you talk about it, it’s a dream, if you envision it, it’s possible, but if you schedule it, it’s real.” – Tony Robbins


