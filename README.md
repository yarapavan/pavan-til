# Today I Learned

Inspired by TIL subreddit and other github-based TIL models, intend to keep this
as a journal for keeping short notes, concepts, bits of syntax, commands, one-liners
and tips I encounter in my daily dose of learning :)


