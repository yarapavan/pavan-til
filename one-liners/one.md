#### convert images -> jpg to png
for i in *.jpg; convert $i (basename $i .jpg).png; end

#### assign file to a variable
begin; set -l IFS; set data (cat data.txt); end

#### move all .suffix files 
mv *.{c,h} src/

###image optimization with jpegoptim
find . -type f -name "*.jpg" -exec jpegoptim {} \;

