# Ciphers and algorithms supported

### OpenSSH

```
$ ssh -Q cipher
$ ssh -Q cipher-auth
$ ssh -Q mac
$ ssh -Q kex
$ ssh -Q key
```

#### More
[1] https://wiki.mozilla.org/Security/Guidelines/OpenSSH
