# Key Generation

### OpenSSH

#### RSA 

RSA keys favored over ECDSA keys when backward compatibility ''is required''. All newly generated keys are always either ED25519 or RSA (not ECDSA or DSA).

for personal use

```
$ ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa
```
for biz/work

```
$ ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa_$(date +%Y-%m-%d) -C "key for work/biz case"
```

#### ED25519
ED25519 keysfavored over RSA keys when backward compatibility ''is not required''.

for personal use (with OpenSSH 6.4+ and 256 bytes)

```
$ ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519
```
for biz/work

```
$ ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_$(date +%Y-%m-%d) -C "ed25519 key for work/biz case"
```

#### More
[1] https://wiki.mozilla.org/Security/Guidelines/OpenSSH

[2] https://blog.cloudflare.com/ecdsa-the-digital-signature-algorithm-of-a-better-internet/

[3] https://blog.cloudflare.com/a-relatively-easy-to-understand-primer-on-elliptic-curve-cryptography/
